package edu.stevens.decisioncenter.wikieditsdownloader;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.XPathProcessor;
import edu.stevens.decisioncenter.wikieditsdownloader.library.XPathProcessorFactory;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.DatabaseOutputImpl;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.Output;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 *
 */
public class App {

    static Map<Integer, Stack<Pages>> categoriesLevels;
    static List<Pages> pages;
    private static int totalNbLines = 0;
    private static BigInteger minPageId;
    private static String condition = "from pages as p inner join pagesedits as pe on pe.pageid=p.pageid where pe.date between '2012-06-15 00:00:00' and '2012-06-30 00:00:00' ";
    
    
    public static void main(String[] args) {
        Output output = new DatabaseOutputImpl();
        
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(1);
        
        // pagesedits
        // BETWEEN "2012-08-05T09:42:35Z" AND "2012-08-07T11:08:44Z"
                
        //newFixedThreadPool.submit(getPagesEditsProcessor(output, "2012-08-06T01:23:00Z", "2012-08-07T11:08:44Z"));
        
        // entityedits
        // BETWEEN "2012-10-29T21:28:01Z" AND "2012-11-05T15:13:25Z"
        
        newFixedThreadPool.submit(getEntityEditsProcessor(output, "2012-10-29T21:29:00Z", "2012-11-05T15:13:00Z"));
        
        newFixedThreadPool.shutdown();
    }

    private static XPathProcessor getPagesEditsProcessor(Output output, String rcStart, String rcEnd) {
        
        XPathProcessorFactory builder = new XPathProcessorFactory();
        XPathProcessor missingEditsDownloader = builder.getMissingPagesEditsDownloader(output, rcStart, rcEnd); 
        
        return missingEditsDownloader;
    }
    
    private static XPathProcessor getEntityEditsProcessor(Output output, String rcStart, String rcEnd) {
        XPathProcessorFactory factory = new XPathProcessorFactory();
        XPathProcessor missingEditsDownloader = factory.getMissingEntityEditsDownloader(output, rcStart, rcEnd);
        
        return missingEditsDownloader;
    }
}