/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library;

import java.text.MessageFormat;
import java.util.Date;

/**
 *
 * @author michaelpereira
 */
public class Revision {
    private String revid;
    private String parentid;
    private String user;
    private Date timestamp;
    private int size;
    private String comment;
    private boolean minor;
    private boolean anon;

    public Revision(String revid, String parentid, String user, Date timestamp, int size, String comment, boolean minor, boolean anon) {
        this.revid = revid;
        this.parentid = parentid;
        this.user = user;
        this.timestamp = timestamp;
        this.size = size;
        this.comment = comment;
        this.minor = minor;
        this.anon = anon;
    }
    
    @Override
    public String toString() {
        return MessageFormat.format("Revision[revid={0}, user={1}, timestamp={2}, size={3}, comment={4}, minor={5}, anon={6}]",revid, user, timestamp, size, comment, minor, anon);
    }

    
    public String getRevid() {
        return revid;
    }

    public void setRevid(String revid) {
        this.revid = revid;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isMinor() {
        return minor;
    }

    public void setMinor(boolean minor) {
        this.minor = minor;
    }

    public boolean isAnon() {
        return anon;
    }

    public void setAnon(boolean anon) {
        this.anon = anon;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }
}
