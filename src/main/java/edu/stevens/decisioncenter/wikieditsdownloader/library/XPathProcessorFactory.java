/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library;

import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.RecentPagesChangesDownloader;
import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.XPathProcessor;
import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.CategoryDowloader;
import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.WikiRevisionDownloader;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.Output;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.processors.RecentEntitiesChangesDownloader;
import java.text.MessageFormat;

/**
 *
 * @author stevensdecisioncenter
 * 
 * Create and return new instances of the different processors
 * 
 */
public class XPathProcessorFactory {
    
    private final String wikiRevisionDownloaderTargetQuery = "action=query&prop=revisions&format=xml&titles={0}&rvprop=timestamp|user|comment|size|flags|ids&rvlimit=max&rvdir=newer";
        // Excluding following namespaces: *Talk* (1|3|5|7|9|11|13|15|101|109) File(6) MediaWiki(8) Template(10) Help(12) Category(14) ==> &rcnamespace=0|2|4|100|108 ???
    private final String recentChangesDownloaderTargetQuery = "action=query&format=xml&list=recentchanges&rcdir=newer&rctype=edit&rcprop=user|comment|sizes|timestamp|ids|title&rclimit=500&rcstart={0}&rcend={1}";
    private final String categoryDownloaderTargetQuery = "action=query&format=xml&list=categorymembers&cmtitle={0}&cmsort=sortkey&cmdir=asc&cmlimit=max";

    
    public XPathProcessorFactory(){
    }
    
    public XPathProcessor getWikiRevisionDownloader(Pages page){
        if (page == null){
            return null;
        }
        return new WikiRevisionDownloader(page, MessageFormat.format(wikiRevisionDownloaderTargetQuery, page.getTitle()));  
    }
    
    public XPathProcessor getMissingPagesEditsDownloader(Output output, String start, String end){
        return new RecentPagesChangesDownloader(recentChangesDownloaderTargetQuery, output, start, end);
    }
    
    public XPathProcessor getMissingEntityEditsDownloader(Output output, String start, String end){
        return new RecentEntitiesChangesDownloader(recentChangesDownloaderTargetQuery, output, start, end);
    }
    
    public XPathProcessor getCategoryDownloader(String title){
        return new CategoryDowloader(MessageFormat.format(categoryDownloaderTargetQuery, title));
    }
  
}
