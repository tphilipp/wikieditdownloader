/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesedits;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pagesrevisions;

/**
 *
 * @author michaelpereira
 */
public class ConsoleOutputImpl implements Output {

    public void savePageEdit(Pagesedits pageEdit) {
        System.out.println("Edit: " + pageEdit.getPages().getPageid() + " - " + pageEdit.getMessage() + " - " + pageEdit.getDate() + " - " + pageEdit.getAuthor() + " - " + pageEdit.getDiff());
    }

    public void saveRevision(Pagesrevisions r) {
        System.out.println(r.getDate() + " " + r.getRevisionid() + " " + r.getAuthor() + " " + r.getMessage());
    }

    public void savePage(Pages p) {
        System.out.println("Page: " + p.getDate() + " - " + p.getTitle() + " - " + p.getCategories());
    }
}
