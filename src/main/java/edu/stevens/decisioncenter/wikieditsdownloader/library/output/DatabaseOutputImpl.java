/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pagesrevisions;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author michaelpereira
 */
public class DatabaseOutputImpl implements Output {

    private Session session;

    public DatabaseOutputImpl() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public void savePageEdit(Pagesedits pageEdit) {
        try{
            
            if (!session.isOpen()) {
                session = HibernateUtil.getSessionFactory().openSession();
            }
            session.beginTransaction();
            session.save(pageEdit);
            session.getTransaction().commit();
        } catch (Exception e){
            Logger.getLogger(DatabaseOutputImpl.class.getName()).log(Level.SEVERE, "Could not save EDIT", e);
            session.close();
            session = HibernateUtil.getSessionFactory().openSession();
        }
    }

    public void saveRevision(Pagesrevisions r) {
        try{
            if (!session.isOpen()) {
                session = HibernateUtil.getSessionFactory().openSession();
            }
            session.beginTransaction();
            session.save(r);
            session.getTransaction().commit();
        } catch (Exception e){
            Logger.getLogger(DatabaseOutputImpl.class.getName()).log(Level.SEVERE, "Could not save EDIT", e);
            session.close();
            session = HibernateUtil.getSessionFactory().openSession();
        }
    }

    public void savePage(Pages p) {
        try{
            if (!session.isOpen()) {
                session = HibernateUtil.getSessionFactory().openSession();
            }
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();
        } catch (Exception e){
            Logger.getLogger(DatabaseOutputImpl.class.getName()).log(Level.SEVERE, "Could not save EDIT", e);
            session.close();
            session = HibernateUtil.getSessionFactory().openSession();
        }
    }
}
