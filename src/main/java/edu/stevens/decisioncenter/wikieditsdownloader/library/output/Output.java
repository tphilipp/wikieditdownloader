/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesedits;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pagesrevisions;

/**
 *
 * @author michaelpereira
 */
public interface Output {
    void savePageEdit(Pagesedits pageEdit);
    void saveRevision(Pagesrevisions r);
    void savePage(Pages p);
}
