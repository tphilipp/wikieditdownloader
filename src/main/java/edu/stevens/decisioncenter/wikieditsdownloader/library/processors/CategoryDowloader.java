/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.Output;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pages2;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pagesrevisions;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.text.ParseException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.util.Stack;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;

/**
 *
 * @author stevensdecisioncenter
 */
public class CategoryDowloader extends GenericProcessor{
    
    private Logger logger = Logger.getLogger(CategoryDowloader.class);
    
    private List<Pages2> pages = new ArrayList<Pages2>();
    private Stack<Pages2> categories = new Stack<Pages2>();
    private Collection<Pages2> pageList = null;

    
    public CategoryDowloader(String targetQuery, Output output){
        super(targetQuery, output);
        
    }
    
    public CategoryDowloader(String targetQuery){
        super(targetQuery);
    }

    @Override
    protected void retrieveNodesContent(XPath xpath, NodeList nodes) throws XPathExpressionException, ParseException {
        pageList = new ArrayList<Pages2>();
        XPathExpression titleExpr = xpath.compile("@title");
        XPathExpression pageidExpr = xpath.compile("@pageid");
        XPathExpression nsExpr = xpath.compile("@ns");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node currentNode = nodes.item(i);
            final String pageId = (String) pageidExpr.evaluate(currentNode, XPathConstants.STRING);
            final String title = (String) titleExpr.evaluate(currentNode, XPathConstants.STRING);
            final String ns = (String) nsExpr.evaluate(currentNode, XPathConstants.STRING);
            Pages2 page = new Pages2(title, ns, new HashSet <Pagesrevisions>());
            pageList.add(page);
        }
    }

    @Override
    protected void setNodeNameToBeProcessed() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void mainProcess() {
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            Document doc = getXMLDocument(targetQuery.concat("&cmlimit=max"));
            processContent(xpath, doc);

            while ((Boolean) xpath.evaluate("boolean(//query-continue/categorymembers)", doc, XPathConstants.BOOLEAN)) {
                XPathExpression queryContinueExpr = xpath.compile("//query-continue/categorymembers/@cmcontinue");
                String continueValue = (String) queryContinueExpr.evaluate(doc, XPathConstants.STRING);
                doc = getXMLDocument(targetQuery.concat(MessageFormat.format("&cmlimit=max&cmcontinue={0}", continueValue)));
                processContent(xpath, doc);
            }

        } catch (ParseException ex) {
            handleErrorMessage(ex);
        } catch (XPathExpressionException ex) {
            handleErrorMessage(ex);
        } catch (URISyntaxException ex) {
            handleErrorMessage(ex);
        } catch (SAXException ex) {
            handleErrorMessage(ex);
        } catch (ParserConfigurationException ex) {
            handleErrorMessage(ex);
        } catch (MalformedURLException ex) {
            handleErrorMessage(ex);
        } catch (IOException ex) {
            handleErrorMessage(ex);
        }
    }
    
    @Override
    public void processContent(XPath xpath, Document doc) throws XPathExpressionException, ParseException {
        this.nodeNameToBeProcessed = MessageFormat.format("//cm[@ns={0}]", "0");
        NodeList articleNodes = retrieveNodes(xpath, doc);
        this.nodeNameToBeProcessed = MessageFormat.format("//cm[@ns={0}]", "14");
        NodeList categoryNodes = retrieveNodes(xpath, doc);
        retrieveNodesContent(xpath, articleNodes);
        pages.addAll(pageList);
        retrieveNodesContent(xpath, categoryNodes);
        categories.addAll(pageList);
    }
 
    public Stack<Pages2> getCategories() {
        return categories;
    }

    public void setCategories(Stack<Pages2> categories) {
        this.categories = categories;
    }

    @Override
    protected void logErrorMessage(Throwable t) {
        logger.error(t.getMessage(), t);
    }
}