/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.ConsoleOutputImpl;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.Output;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author stevensdecisioncenter
 * 
 * Generic class containing main methods common to every processor handling
 * Wikipedia data.
 * Also defines methods that need to be implement in the subclasses
 * 
 */
public abstract class GenericProcessor implements XPathProcessor {
    
    protected Output output;
    // query used to request data from Wikipedia
    protected String targetQuery;
    // name of the researched nodes in the XML Document representing Wikipedia
    // response
    protected String nodeNameToBeProcessed;
    
    public GenericProcessor(String targetQuery){
        this.targetQuery = targetQuery;
        this.output = new ConsoleOutputImpl();
        setNodeNameToBeProcessed();
    }
    
    public GenericProcessor(String targetQuery, Output output){
        this(targetQuery);
        this.output = output;
    }    
    
    public void processContent(XPath xpath, Document doc) throws XPathExpressionException, ParseException {
        NodeList nodes = retrieveNodes(xpath, doc);
        retrieveNodesContent(xpath, nodes);
    }
        
    protected Document getXMLDocument(String targetQuery) throws URISyntaxException, MalformedURLException, IOException, ParserConfigurationException, SAXException {
        // Request daa from Wikipedia
        URI uri = new URI("http", "en.wikipedia.org", "/w/api.php", targetQuery, null);
        URL url = uri.toURL();
        url.openConnection();
        InputStream inputStream = url.openStream();

        // Transform Wikipedia response into a XML document
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse(inputStream);
    }
    
    /**
     * Return the list of nodes relevant to the current research and found in
     * the XML document.
     * 
     * @param xpath
     * @param doc
     * @return
     * @throws XPathExpressionException 
     */
    protected NodeList retrieveNodes(XPath xpath, Document doc) throws XPathExpressionException {
        // Create an XPath expression with the relevant node name
        XPathExpression expr = xpath.compile(this.nodeNameToBeProcessed);
        // Apply this XPath expression to the XML document given in parameter
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        // Return the matching nodes
        return (NodeList) result;
    }

    // Method called when processor is used in a thread
    public void run() {
        mainProcess();
    }
      
    /**
     * Generic method handling code exceptions.
     * Log the error and send an email to developers.
     * 
     * @param t 
     */
    protected void handleErrorMessage(Throwable t){
        logErrorMessage(t);
        sendErrorMessage(t);
    }

    /**
     * Method that sends an email containing the stack  trace of the exception
     * given in parameter.
     * 
     * @param t 
     */
    private void sendErrorMessage(Throwable t){
        try {
            String username = "tphilipp";
            String password = "";
            String host = "nexus.stevens.edu";
            String from = "tphilipp@stevens.edu";
            String to = "tphilipp@stevens.edu";

            // Get system properties
            Properties props = System.getProperties();

            // Setup mail server
            props.put("mail.smtp.host", host);

            // Get session
            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, 
              new InternetAddress(to));
            message.setSubject("Wikipedia edits error");
            message.setContent("WIKIPEDIA EDITS Error. Check error message below: \n" + getStackTrace(t), "text/plain");

            // Send message
            Transport transport = session.getTransport("smtp");
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException ex) {
            logErrorMessage(ex);

        }
    }

    /**
     * Transform Stack trace into a readable string
     * 
     * @param aThrowable
     * @return 
     */
    private String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    /**
     * Iterate over the XML nodes given in parameter and retrieve the
     * content relevant for the current research
     * 
     * @param xpat
     * @param nodes: List of XML nodes
     * @throws XPathExpressionException
     * @throws ParseException 
     */
    protected abstract void retrieveNodesContent(XPath xpath, NodeList nodes) throws XPathExpressionException, ParseException;
    
    /**
     * Define the name of the nodes relevant to the current research.
     */
    protected abstract void setNodeNameToBeProcessed();
    
    /**
     * Log error given in parameter.
     * 
     * @param t 
     */
    protected abstract void logErrorMessage(Throwable t);
}