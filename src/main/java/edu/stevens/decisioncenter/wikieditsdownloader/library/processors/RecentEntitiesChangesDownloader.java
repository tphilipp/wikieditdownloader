/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.Output;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Entity;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.EntityCategories;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.EntityEdits;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author stevensdecisioncenter
 * 
 */
public class RecentEntitiesChangesDownloader extends RecentPagesChangesDownloader {
    
    private Logger logger = Logger.getLogger(RecentEntitiesChangesDownloader.class);
    
    public RecentEntitiesChangesDownloader(String targetQuery, String startDate, String endDate){
        super(targetQuery, startDate, endDate);
    }
    
    public RecentEntitiesChangesDownloader(String targetQuery, Output output, String startDate, String endDate){
        super(targetQuery, output, startDate, endDate);
    }

    @Override
    protected void retrieveNodesContent(XPath xpath, NodeList nodes) throws XPathExpressionException, ParseException {
        // Create Xpath expressions for all the nodes we are going to work on
        XPathExpression entityIdExpr = xpath.compile("@pageid");
        XPathExpression titleExpr = xpath.compile("@title");
        XPathExpression timestampExpr = xpath.compile("@timestamp");
        XPathExpression commentExpr = xpath.compile("@comment");
        XPathExpression oldLenExpr = xpath.compile("@oldlen");
        XPathExpression newLenExpr = xpath.compile("@newlen");
        XPathExpression userExpr = xpath.compile("@user");

        session = HibernateUtil.getSessionFactory().openSession();

        int nbNodes = nodes.getLength();
        
        Entity entity = null;
        List<Entity> entityCategories = null;
        List<EntityCategories> entityCategoriesToSave =null;
        for (int i = 0; i < nbNodes; i++) {
            try {            
                // Retrieve the content of the nodes from the XML document
                Node currentNode = nodes.item(i);
                String entityId = (String) entityIdExpr.evaluate(currentNode, XPathConstants.STRING);
                String title = (String) titleExpr.evaluate(currentNode, XPathConstants.STRING);
                String timestamp = (String) timestampExpr.evaluate(currentNode, XPathConstants.STRING);
                String comment = (String) commentExpr.evaluate(currentNode, XPathConstants.STRING);
                String oldLen = (String) oldLenExpr.evaluate(currentNode, XPathConstants.STRING);
                String newLen = (String) newLenExpr.evaluate(currentNode, XPathConstants.STRING);
                String user = (String) userExpr.evaluate(currentNode, XPathConstants.STRING);

                // Skip node if it matches a selected group of titles
                if (title.contains("User")
                        || title.contains("Wikipedia")
                        || title.contains("File")
                        || title.contains("MediaWiki")
                        || title.contains("Template")
                        || title.contains("Help")
                        || title.contains("Category")
                        || title.contains("Special")
                        || title.contains("talk")
                        || title.contains("Talk")) {
                    continue;
                }
                totalNbNodes++;

                // Remove spaces from title
                title = title.replace(' ', '_');
                // Don't use method load because of problems lazy loading underlying categories
                entity = (Entity) session.createQuery("select e from Entity e where e.id = ?").setString(0, entityId).uniqueResult();
                Document xmlDocument = getXmlDocumentForUrl("http://en.wikipedia.org/w/api.php?action=query&format=xml&prop=info|categories&pageids=" + entityId);
                
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                // Create a new entity if not already in the database
                if (entity == null) {

                    // Handle last touched date
                    String lastTouchedDate = (String) getPageAttributeFromXmlDocument(xmlDocument, "touched");
                    lastTouchedDate = lastTouchedDate.replace('T', ' ');
                    lastTouchedDate = lastTouchedDate.replace('Z', ' ');
                
                    entity = new Entity(Long.valueOf(entityId), title, String.valueOf(false), sdf.parse(lastTouchedDate), new Date());
                    // table EntityEdits
                    session.save(entity);
                }
                
                // table EntityEdits
                entityCategories = getEntityCategoriesFromXmlDocument(xmlDocument, entityId);
                
                // Iterate over all the associated categories (old and new) and 
                // check if any of them is a disambiguation one
                boolean disam = false;
                for (Object o : entity.getEntityCategoriesForEntityid()) {
                    EntityCategories ec = (EntityCategories) o;
                    disam = disam || isDisambiguationEntity(ec.getEntityByCategoryid());
                }
                // Associate new categories to the current entity
                for (Entity category : entityCategories){
                    disam = disam || isDisambiguationEntity(category);
                    entity.getEntityCategoriesForEntityid().add(new EntityCategories(null, entity, category));
                }
                
                entity.setDisam(String.valueOf(disam));
                
                int diff = Integer.valueOf(oldLen).intValue() - Integer.valueOf(newLen).intValue();

                timestamp = timestamp.replace('T', ' ');
                timestamp = timestamp.replace('Z', ' ');
                Date editDate = sdf.parse(timestamp);
                
                EntityEdits newEdit = new EntityEdits(entity.getId(), editDate, comment, diff, user);
                
                // Update entity with possible new associated categories
                session.update(entity);
                // Save current edit
                session.save(newEdit);
                session.flush();
                
            } catch (ParseException ex) {
                handleErrorMessage(ex);
            } catch (Exception ex) {
                handleErrorMessage(ex);
            }
        }

        logger.info("Not found / Edits handled = " + getTotalNbPagesNotFound() + " / " + getTotalNbNodes());

        session.close();
    }

    /**
     * Handle code errors.
     * Log the error, sends an email to developers, close and reopen Hibernate
     * session to avoid future Hibernate wrong behaviors
     * 
     * @param t 
     */
    @Override
    protected void handleErrorMessage(Throwable t) {
        super.handleErrorMessage(t);
        session.close();
        session = HibernateUtil.getSessionFactory().openSession();
    }
    
    private List<Entity> getEntityCategoriesFromXmlDocument(Document xmlDocument, String revisionId) throws XPathExpressionException, Exception{
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        
        // Get the XML nodes for all catefories
        XPathExpression catgeroyExpr = xpath.compile("//cl");
        NodeList categoryNodes = (NodeList) catgeroyExpr.evaluate(xmlDocument, XPathConstants.NODESET);

        List<Entity> categories = new ArrayList<Entity> ();
        XPathExpression categoryTitleExpr = xpath.compile("@title");
                       
        for (int i = 0; i < categoryNodes.getLength(); i++) {
            // Retrieve the value of the "title" attribute of the category
            String categoryTitle = (String) categoryTitleExpr.evaluate(categoryNodes.item(i), XPathConstants.STRING);
            categoryTitle = categoryTitle.replace(' ', '_');

            // Check tif the category is already in the database
            Entity category = null;
            try{
                category = (Entity) session.createQuery(
                "select n from Entity as n where n.title = ?").setString(0, categoryTitle).uniqueResult();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (category == null){
                // Create the new category that is not in the database
                try {
                    Document categoryXmlDoc = getXmlDocumentForUrl("http://en.wikipedia.org/w/api.php?action=query&format=xml&prop=info&titles=" + categoryTitle);
                    
                    String pageId = (String) getPageAttributeFromXmlDocument(categoryXmlDoc, "pageid");
                    
                    if (pageId == null || pageId.equals("")){
                        continue;
                    }
                    
                    category = (Entity) session.createQuery(
                    "select n from Entity as n where n.id = ?").setString(0, String.valueOf(pageId)).uniqueResult();
                    
                    if (category == null){
                  
                        String lastTouchedDate = (String) getPageAttributeFromXmlDocument(categoryXmlDoc, "touched");
                        lastTouchedDate = lastTouchedDate.replace('T', ' ');
                        lastTouchedDate = lastTouchedDate.replace('Z', ' ');
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        category = new Entity(Long.valueOf(pageId), categoryTitle, String.valueOf(false), sdf.parse(lastTouchedDate), new Date());

                        session.save(category);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            categories.add(category);
        }
        
        return categories;     
    }
    
    /**
     * Check wether the entity given in parameter is a disambiguation one or not
     * 
     * @param entity
     * @return true if the entity given in parameter is a diambiguation entity, false otherwise
     */
    private boolean isDisambiguationEntity(Entity entity){
        if (entity == null || entity.getTitle() == null){
            return false;
        }
        
        String title = entity.getTitle();
        title = title.toLowerCase().replace("category:", "");
        // Return true if entity title matches a given set of title representing
        // all existing disambiguation entities (Wikipedia pages)
        return title.equalsIgnoreCase("Disambiguation pages") ||
                title.equalsIgnoreCase("All article disambiguation pages") ||
                title.equalsIgnoreCase("All disambiguation pages") ||
                title.equalsIgnoreCase("Disambiguation_pages") ||
                title.equalsIgnoreCase("All_article_disambiguation_pages") ||
                title.equalsIgnoreCase("All_disambiguation_pages");
    }

    @Override
    protected void logErrorMessage(Throwable t) {
        logger.error(t.getMessage(), t);
    }
}