/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.HibernateUtil;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesedits;
import java.io.*;
import java.net.*;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 *
 * @author michaelpereira
 * 
 * REquest data from Wikipedia and saves it in the old database model
 * 
 */
public class RecentPagesChangesDownloader extends GenericProcessor {
    
    private Logger logger = Logger.getLogger(RecentPagesChangesDownloader.class);
    
    private String startDate;
    private String endDate;

    private List<Pagesedits> pagesEdits = new ArrayList<Pagesedits>();
    
    protected Session session;
    protected int totalNbNodes = 0;
    protected int totalNbPagesNotFound = 0;
    
    public RecentPagesChangesDownloader(String targetQuery, String startDate, String endDate){
        super(targetQuery);
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public RecentPagesChangesDownloader(String targetQuery, Output output, String startDate, String endDate){
        super(targetQuery, output);
        this.startDate = startDate;
        this.endDate = endDate;
    }
       
    /**
     * Extract categories from the XML document given in parameter.
     * Return the list of all theses categories.
     * 
     * @param xmlDocument
     * @param revisionId
     * @return
     * @throws XPathExpressionException
     * @throws Exception 
     */
    private List<Pages> getCategoriesFromXmlDocument(Document xmlDocument, String revisionId) throws XPathExpressionException, Exception{
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        
        // Get the XML nodes for all catefories
        XPathExpression catgeroyExpr = xpath.compile("//cl");
        NodeList categoryNodes = (NodeList) catgeroyExpr.evaluate(xmlDocument, XPathConstants.NODESET);

        List<Pages> categories = new ArrayList<Pages> ();
        XPathExpression categoryTitleExpr = xpath.compile("@title");
                       
        for (int i = 0; i < categoryNodes.getLength(); i++) {
            // Retrieve the value of the "title" attribute of the category
            String categoryTitle = (String) categoryTitleExpr.evaluate(categoryNodes.item(i), XPathConstants.STRING);
            categoryTitle = categoryTitle.replace(' ', '_');

            // Check tif the category is already in the database
            Pages category = (Pages) session.createQuery(
                    "select n from Pages as n where n.title = ?").setString(0, categoryTitle).uniqueResult();
            
            // Create a new object if it's not already in the database
            // These objects will be saved outside this method
            if (category == null){
                // Create the new category that is not in the database
                try {
                    Document categoryXmlDoc = getXmlDocumentForUrl("http://en.wikipedia.org/w/api.php?action=query&format=xml&prop=info&titles=" + categoryTitle);
                    String lastTouchedDate = (String) getPageAttributeFromXmlDocument(categoryXmlDoc, "touched");
                    lastTouchedDate = lastTouchedDate.replace('T', ' ');
                    lastTouchedDate = lastTouchedDate.replace('Z', ' ');
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    
                    // table page
                    category = new Pages(categoryTitle, new Date()); 
                } catch (Exception e) {
                    handleErrorMessage(e);
                }
            }
            
            categories.add(category);
        }
        
        return categories;     
    }

    /**
     * Retrieve the value of the attribute in the node "page" of the XML 
     * document given in parameter.
     * 
     * @param xmlDocument
     * @param attribute
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XPathExpressionException
     * @throws Exception 
     */
    public Object getPageAttributeFromXmlDocument(Document xmlDocument, String attribute) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, Exception{
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        // Get the XML node containing the page info
        XPathExpression pageExpr = xpath.compile("//page");
        NodeList pageNodes = (NodeList) pageExpr.evaluate(xmlDocument, XPathConstants.NODESET);
        // Exactly 1 page Node expected for a given revisionId
        if (pageNodes == null) {
            logger.error("No page found for the page id");
            return "";
        }
        if (pageNodes.getLength() != 1){
            logger.error(pageNodes.getLength() + " pages found for the page id. Should have found only one");
            return "";
        }

        XPathExpression pageIdExpr = xpath.compile("@" + attribute);
        Node node = pageNodes.item(0);
        // Return the value of the attribute from the info stored in the "page" node
        return pageIdExpr.evaluate(node, XPathConstants.STRING);        
    }

    /**
     * Connect to the URL given in parameter an return the XML document 
     * corresponding to the connection response
     * 
     * @param url
     * @return
     * @throws IOException
     * @throws Exception 
     */
    protected Document getXmlDocumentForUrl(String url) throws IOException, Exception{
        HttpURLConnection urlConnection = getUrlConnection(url);
        if (urlConnection == null || urlConnection.getResponseCode() != 200){
            System.err.println("Could not connect to URL " + url);
            return null;
        }

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();

        // Build the XML document from the result of the URL
        Document doc = builder.parse(urlConnection.getInputStream());

        return doc;
    }

    /**
     * Return the opened connection for the URL given in parameter
     * 
     * @param wikiUrl
     * @return
     * @throws IOException 
     */
    private HttpURLConnection getUrlConnection(String wikiUrl) throws IOException{
        if (wikiUrl == null){
            return null;
        }
        URL url = new URL(wikiUrl);
        return (HttpURLConnection) url.openConnection();
    }

    /**
     * @return the pagesEdits
     */
    public List<Pagesedits> getPagesEdits() {
        return pagesEdits;
    }

    /**
     * @param pagesEdits the pagesEdits to set
     */
    public void setPagesEdits(List<Pagesedits> pagesEdits) {
        this.pagesEdits = pagesEdits;
    }

    /**
     * @return the totalNbNodes
     */
    public int getTotalNbNodes() {
        return totalNbNodes;
    }

    /**
     * @return the totalNbPagesNotFound
     */
    public int getTotalNbPagesNotFound() {
        return totalNbPagesNotFound;
    }

    /**
     * Request  and process recent changes from Wikipedia (in chronological 
     * order). Iterate over these recent changes until the end date is reached.
     */
    public void mainProcess() {
                try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            Document doc = getXMLDocument(MessageFormat.format(targetQuery, startDate, endDate));
            processContent(xpath, doc);

            while ((Boolean) xpath.evaluate("boolean(//query-continue/recentchanges)", doc, XPathConstants.BOOLEAN)) {
                XPathExpression queryContinueExpr = xpath.compile("//query-continue/recentchanges/@rcstart");
                String continueValue = (String) queryContinueExpr.evaluate(doc, XPathConstants.STRING);

                doc = getXMLDocument(MessageFormat.format(targetQuery, continueValue, endDate));
                logger.info("NEW INTERVAL = " + continueValue + " / " + endDate);
                processContent(xpath, doc);
            }

        } catch (ParseException ex) {
            handleErrorMessage(ex);
        } catch (XPathExpressionException ex) {
            handleErrorMessage(ex);
        } catch (URISyntaxException ex) {
            handleErrorMessage(ex);
        } catch (SAXException ex) {
            handleErrorMessage(ex);
        } catch (ParserConfigurationException ex) {
            handleErrorMessage(ex);
        } catch (MalformedURLException ex) {
            handleErrorMessage(ex);
        } catch (IOException ex) {
            handleErrorMessage(ex);
        }
    }
    
    @Override
    protected void setNodeNameToBeProcessed() {
        this.nodeNameToBeProcessed = "//rc";
    }
    
    @Override
    protected void retrieveNodesContent(XPath xpath, NodeList nodes) throws XPathExpressionException, ParseException {
        // Create XPath expressions for all the attributes that are going to be
        // extracted from each node of the given node list
        XPathExpression pageIdExpr = xpath.compile("@pageid");
        XPathExpression titleExpr = xpath.compile("@title");
        XPathExpression timestampExpr = xpath.compile("@timestamp");
        XPathExpression commentExpr = xpath.compile("@comment");
        XPathExpression oldLenExpr = xpath.compile("@oldlen");
        XPathExpression newLenExpr = xpath.compile("@newlen");
        XPathExpression userExpr = xpath.compile("@user");

        session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        int nbNodes = nodes.getLength();
        for (int i = 0; i < nbNodes; i++) {
            try {
                Node currentNode = nodes.item(i);
                // Get the attributes' values of the given node
                String pageId = (String) pageIdExpr.evaluate(currentNode, XPathConstants.STRING);
                String title = (String) titleExpr.evaluate(currentNode, XPathConstants.STRING);
                String timestamp = (String) timestampExpr.evaluate(currentNode, XPathConstants.STRING);
                String comment = (String) commentExpr.evaluate(currentNode, XPathConstants.STRING);
                String oldLen = (String) oldLenExpr.evaluate(currentNode, XPathConstants.STRING);
                String newLen = (String) newLenExpr.evaluate(currentNode, XPathConstants.STRING);
                String user = (String) userExpr.evaluate(currentNode, XPathConstants.STRING);

                // Skip the node if it matches a given set of titles
                if (title.contains("User")
                        || title.contains("Wikipedia")
                        || title.contains("File")
                        || title.contains("MediaWiki")
                        || title.contains("Template")
                        || title.contains("Help")
                        || title.contains("Category")
                        || title.contains("Special")
                        || title.contains("talk")
                        || title.contains("Talk")) {
                    continue;
                }
                totalNbNodes++;

                title = title.replace(' ', '_');
                // Look for an already existing Page matching the title attribute
                Pages page = (Pages) session.createQuery(
                        "select n from Pages as n where n.title = ?").setString(0, "http://en.wikipedia.org/w/index.php?action=raw&title=" + title).uniqueResult();
                Document xmlDocument = getXmlDocumentForUrl("http://en.wikipedia.org/w/api.php?action=query&format=xml&prop=info|categories&pageids=" + pageId);
                
                // Create and save a new one if no page already found in DB
                if (page == null) {

                    // table pagesedits
                    // Get all the categories and build the matching text field
                    List<Pages> categories = getCategoriesFromXmlDocument(xmlDocument, pageId);
                    String pageCategories = "";
                    for (Pages c : categories) {
                        pageCategories += c.getTitle().replace('_', ' ').replaceAll("Category:", "") + "~";
                    }
                    
                    page = new Pages("http://en.wikipedia.org/w/index.php?action=raw&title=" + title, new Date(), pageCategories);
                    session.save(page);
                }
                
                // Create the current page edit and save edit
                
                // Handle last touched date
                String lastTouchedDate = (String) getPageAttributeFromXmlDocument(xmlDocument, "touched");
                lastTouchedDate = lastTouchedDate.replace('T', ' ');
                lastTouchedDate = lastTouchedDate.replace('Z', ' ');
                
                Pagesedits newEdit = new Pagesedits();
                newEdit.setPages(page);
                newEdit.setMessage(comment);
                newEdit.setAuthor(user);

                int diff = Integer.valueOf(oldLen).intValue() - Integer.valueOf(newLen).intValue();
                newEdit.setDiff(diff);

                timestamp = timestamp.replace('T', ' ');
                timestamp = timestamp.replace('Z', ' ');
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = sdf.parse(timestamp);
                newEdit.setDate(date);

                session.save(newEdit);

            } catch (ParseException ex) {
                handleErrorMessage(ex);
            } catch (Exception ex) {
                handleErrorMessage(ex);
            }
        }

        logger.info("Not found / Edits handled = " + getTotalNbPagesNotFound() + " / " + getTotalNbNodes());

        session.getTransaction().commit();
        session.close();
    }

    @Override
    protected void logErrorMessage(Throwable t) {
        logger.error(t.getMessage(), t);
    }
}
