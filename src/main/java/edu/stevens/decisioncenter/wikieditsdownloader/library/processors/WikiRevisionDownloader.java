/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.old.Pagesrevisions;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 *
 * @author Michael
 */
public class WikiRevisionDownloader extends GenericProcessor {
    
    private Logger logger = Logger.getLogger(WikiRevisionDownloader.class);

    private Pages page;
    
    public WikiRevisionDownloader(Pages page, String targetQuery, Output output) {
        super(targetQuery, output);
        this.page = page;
    }
    
    public WikiRevisionDownloader(Pages page, String targetQuery){
        super(targetQuery);
        this.page = page;
    }

    public void mainProcess() {
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            Document doc = getXMLDocument(targetQuery.concat("&rvstart=1262304000"));
            processContent(xpath, doc);

            while ((Boolean) xpath.evaluate("boolean(//query-continue/revisions)", doc, XPathConstants.BOOLEAN)) {
                XPathExpression queryContinueExpr = xpath.compile("//query-continue/revisions/@rvstartid");
                String continueValue = (String) queryContinueExpr.evaluate(doc, XPathConstants.STRING);
                doc = getXMLDocument(targetQuery.concat(MessageFormat.format("&rvstartid={0}", continueValue)));
                processContent(xpath, doc);
            }

        } catch (ParseException ex) {
            handleErrorMessage(ex);
        } catch (XPathExpressionException ex) {
            handleErrorMessage(ex);
        } catch (URISyntaxException ex) {
            handleErrorMessage(ex);
        } catch (SAXException ex) {
            handleErrorMessage(ex);
        } catch (ParserConfigurationException ex) {
            handleErrorMessage(ex);
        } catch (MalformedURLException ex) {
            handleErrorMessage(ex);
        } catch (IOException ex) {
            handleErrorMessage(ex);
        }
    }
    
    protected void retrieveNodesContent(XPath xpath, NodeList articleNodes) throws XPathExpressionException, ParseException {
        XPathExpression timestampExpr = xpath.compile("@timestamp");
        XPathExpression commentExpr = xpath.compile("@comment");
        XPathExpression revisionIdExpr = xpath.compile("@revid");
        XPathExpression parendIdExpr = xpath.compile("@parentid");
        XPathExpression userExpr = xpath.compile("@user");
        XPathExpression sizeExpr = xpath.compile("@size");
        XPathExpression anonExpr = xpath.compile("boolean(@anon)");
        XPathExpression minorExpr = xpath.compile("boolean(@size)");

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        for (int i = 0; i < articleNodes.getLength(); i++) {
            Node currentNode = articleNodes.item(i);
            final String timestamp = (String) timestampExpr.evaluate(currentNode, XPathConstants.STRING);
            final Date revDate = inputFormat.parse(timestamp);
            final String comment = (String) commentExpr.evaluate(currentNode, XPathConstants.STRING);
            final Double revisionId = (Double) revisionIdExpr.evaluate(currentNode, XPathConstants.NUMBER);
            final Double parentId = (Double) parendIdExpr.evaluate(currentNode, XPathConstants.NUMBER);
            final String user = (String) userExpr.evaluate(currentNode, XPathConstants.STRING);
            final Double size = (Double) sizeExpr.evaluate(currentNode, XPathConstants.NUMBER);
            
            final Boolean anon = (Boolean) anonExpr.evaluate(currentNode, XPathConstants.BOOLEAN);
            final Boolean minor = (Boolean) minorExpr.evaluate(currentNode, XPathConstants.BOOLEAN);

            Pagesrevisions rev = new Pagesrevisions();
            rev.setAnon(anon);
            rev.setAuthor(user);
            rev.setDate(revDate);
            rev.setMessage(comment);
            rev.setMinor(minor);
            rev.setParentid(parentId.longValue());
            rev.setRevisionid(revisionId.longValue());
            rev.setSize(size.intValue());
            rev.setPages(page);
            //page.getPagesrevisionses().add(rev);
            output.saveRevision(rev);
        }
    }

    @Override
    protected void setNodeNameToBeProcessed() {
        this.nodeNameToBeProcessed = "//rev";
    }

    @Override
    protected void logErrorMessage(Throwable t) {
        logger.error(t.getMessage(), t);
    }
}