/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.processors;

import java.text.ParseException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;

/**
 *
 * @author stevensdecisioncenter
 * 
 * Behavior of multi-threaded processors applied on XML document
 */
public interface XPathProcessor extends Runnable {
    
    /**
     * Process the XML document given in parameter using the XPath expressions
     * built with the XPath parameter.
     * 
     * @param xpath
     * @param doc: XML Document
     * @throws XPathExpressionException
     * @throws ParseException 
     */
    public void processContent(XPath xpath, Document doc) throws XPathExpressionException, ParseException;
    
    public void mainProcess();
    
}
